Rails.application.routes.draw do
    get 'welcome/index'
    get 'property', to: "property#index"

    root 'welcome#index'
end
