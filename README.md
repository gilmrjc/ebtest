# Super Portal - UI Developer Candidate Project
## Overview
Your mission if you choose to accept it is to convert two web page designs into an actual web pages in a Rails project. We'll be reviewing the pages for how well you structure your HTML, CSS and even maybe your ability to use a bit of JavaScript. You might want to read up a little on where to put your CSS or ahem maybe SCSS in how to structure your files in rails project.

## Getting Started
We used the [Getting Started with Rails](https://guides.rubyonrails.org/getting_started.html) instructions to set up a new project in a repo to speed up your setup. You'll still need clone this repo and install

- [Ruby](https://www.ruby-lang.org/es/documentation/installation/) (2.6.3)
- [Bundler](https://bundler.io/gemfile.html)

on your local machine and then run 

`bundle install`

To install all the other required gems. You can then start a server which you can access at [http://localhost:3000/]() by running

`bundle exec rails server`

## Web Page Designs
The two designs that you need to page layout are:
- [Home Page](mockups/home-page.png)
- [Property Page](mockups/property-page.png)

In both pages there are carousels and we recommend you use the [Owl Carousel](https://owlcarousel2.github.io/OwlCarousel2/) which is already installed in the vendor directory.

### Home Page
The home page consists of the following:
1. Contact header with a search input
2. Menu
3. Hero image with your 2 main select filters with the following options:
    - Property operations: Sale, Rent and Vacation Rentals
    - Property Types: Apartment, Building, House, Land and Office
4. 3x2 grid of featured properties
5. An agent carousel with navigation arrows: [GIF](http://g.recordit.co/9B9bzxw1HZ.gif)
6. Contact footer

### Property Page
The property page consists of the following:
1. Contact header with a search input
2. Menu
3. Title and address section
4. Property images carousel with navigation arrows: [GIF](http://g.recordit.co/Ydct37Ak4g.gif)
5. Property details section with a contact form
6. Property description and amenities section
7. Responsive map
8. 3x1 grid of similar properties
9. Contact footer

## Style Requirements
Make sure you code meets all the [style guide](style-guide.md) requirements.

## Deliverables
You should deliver the project sharing a link to your fork. Keep in mind that the code you deliver should be considered high quality and ready to be used in a production environment and reviewed by your peers. We'll review the code to see if it's clean, clear and uses good practices. Also be sure to replace the notes section below with your own notes.

## Notes

Durante el desarrollo de la prueba me percate de varios detalles relacionados
con el diseño. Entre los más importantes se pueden notar los siguientes:

- La paleta de colores proporcionada no corresponde con los usados en el diseño.
    En particular el color de fondo. Para adaptarlo se cambio el color "gris
    claro" por uno más claro para sustituir este color faltate.
- La tipografía recomendada no corresponde con el diseño. Aunque es parecida,
    eso hace que muchos detalles relacionados con el tamaño y las proporciones
    hayan cambiado.
- Continuando con la tipografía, las fuentes sugeridas no contiene los pesos
    necesarios para el diseño. En particular la fuenta usada dentro del texto no
    incluye una variante "light", y la misma variante en FontAwesome es de
    paga, por lo que el diseño final posee mucho contenido de negro, al no poder
    usar las versiones más ligeras.

Se logró duplicar el diseño, pero con algunas deficiencias por lo comentado
anteriormente. Una idea que se me ocurrió pero no logré implementar por el
tiempo es cambiar la opacidad para simular una fuente más ligera.

Otro detalle que no estaba marcado como requisito pero que se empezó a trabajar
es la mejora del sitio para responsive. El mayor problema que existe se
relaciona con la cabecera. Esta no se transformo en un menú de hamburguesa, por
lo que no está listo para dispositivos mobiles. La mayoría de los demás
elementos si son funcionales al ser vistos desde un celular.

Como entregable anexo, se incluyen las capturas de pantalla del maquetado dentro
de la carpeta de mockups.

